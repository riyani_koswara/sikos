<!DOCTYPE html>
<html>
<head>
	<title>Curiculum Vitae</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" type="text/css">
	 <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
	<style type="text/css">
		#main-navbar .nav-link{
			border-bottom: 5px solid #f9f9f9;
			color: black;
		}
		#main-navbar .nav-link:hover,  #main-navbar a.active{
			border-bottom: 5px solid #f94c27;
			color: #999999
		}
		.main-logo{
			height: 100px;
			margin-bottom: -70px;
		}
		.small-logo{
			height: 60px;
		}
		#showcase{
			background: url("yiha.jpg");
			height: 730px;
			padding-top: 150px;
			background-size: cover;
			background-attachment: fixed;
			background-repeat: no-repeat;
			}
		#showcase .dark-overlay{
			background-color: rgba(0,0,0,0.5);
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 644px;
		}
	</style>
</head>
<body>
	<nav id="main-navbar" class="navbar navbar-expand-md navbar-light bg-light fixed-top py-2 py-md-0">
		<div class="collapse  navbar-collapse" id="navbarNav">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link p-4 active">Home</a>
				</li>
				<li class="nav-item">
					<a class="nav-link p-4" href="#About">About</a>
				</li>
				<li class="nav-item">
					<a class="nav-link p-4" href="#Skill">Skill</a>
				</li>
				<li class="nav-item">
					<a class="nav-link p-4" href="#Portofolio">Portofolio</a>
				</li>
				<li class="nav-item">
					<a class="nav-link p-4" href="#Contact">Contact</a>
				</li>
			</ul>
		</div>
	</nav>

<section id="showcase" class="py-5">
	<div class="dark-overlay">
		<div class="container">
			<div clss="row">
				<div class="col text-center text-white">
					<h1 class="display-3" style="padding-top: 200px">Riyani<strong> Koswara</strong></h1>
					<p class="lead">Student in Senior High School 1 Bawang</p>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- About -->
<section id="About" class="py-5" text-muted>
	<div class="container">
		<div class="row">
			<div class="col-md-6 pt-5">
				<img src="kos.jpg" style="width: 250px; height: 250px;" alt="intermedia" class="img-fluid mb-3] img-thumbnail">
			</div>
			<div class="col-md-6 py-sm-5 py-md-6">
			<h2>What is Riyani?</h2>
			<p class="lead">
				Nama lengkap saya adalah Riyani Koswara.Lahir di Banjarnegara,24 April 2003.Sedang melaksanakan pendidikan di SMK Negeri 1 Bawang jurusan Rekayasa Perangkat Lunak, Saya merupakan seseorang yang tertarik dengan website dan design. Memiliki hoby menyanyi dan berolahraga. Saya seseorang yang memiliki mimpi setinggi langit. Saya memiliki prinsip bahwa tidak ada yang tidak mungkin di dunia ini selagi masih mau berusaha dan berdoa.
			</p>
		</div>
	</div>
</div>	
</section>




<!-- Skill -->
<section id="Skill" class="bg.dark text-black py-5" text-muted>
	<div class="container">
		<div class="row">
			<div class="rol text-center pt-5 pb-3">
				<h2 style="font-size: 60px; font-family: Bernard MT Condensed;">My Skill</h2>
			</div>
		</div>
	</div>
	<div class="container">
	<h2>HTML
	  <div class="progress">
    <div class="progress-bar" style="width:90%">90%</div></div>
</h2>
</div>
<br>
	<div class="container">
	<h2>CSS
	  <div class="progress">
    <div class="progress-bar" style="width:80%">80%</div></div>
</h2>
</div>	

<div class="container">
	<h2>PHP
	  <div class="progress">
    <div class="progress-bar" style="width:75%">75%</div></div>
</h2>
</div>

<div class="container">
	<h2>Java
	  <div class="progress">
    <div class="progress-bar" style="width:75%">75%</div></div>
</h2>
</div>

<div class="container">
	<h2>JavaScript
	  <div class="progress">
    <div class="progress-bar" style="width:75%">75%</div></div>
</h2>
</div>

<div class="container">
	<h2>Python
	  <div class="progress">
    <div class="progress-bar" style="width:70%">70%</div></div>
</h2>
</div>
				
	</section>

  <!-- Contact Section -->
  <section id="Contact" class="contact-section bg-black">
    <div class="container">

      <div class="row">

        <div class="col-md-4 mb-3 mb-md-0">
          <div class="card py-4 h-100">
            <div class="card-body text-center">
              <i class="fas fa-map-marked-alt text-primary mb-2"></i>
              <h4 class="text-uppercase m-0">Address</h4>
              <hr class="my-4">
              <div class="small text-black-50">Badakarya, Gamblok</div>
            </div>
          </div>
        </div>

        <div class="col-md-4 mb-3 mb-md-0">
          <div class="card py-4 h-100">
            <div class="card-body text-center">
              <i class="fas fa-envelope text-primary mb-2"></i>
              <h4 class="text-uppercase m-0">Email</h4>
              <hr class="my-4">
              <div class="small text-black-50">
                <a href="#">riyanikoswara8@gmail.com</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-4 mb-3 mb-md-0">
          <div class="card py-4 h-100">
            <div class="card-body text-center">
              <i class="fas fa-mobile-alt text-primary mb-2"></i>
              <h4 class="text-uppercase m-0">Phone</h4>
              <hr class="my-4">
              <div class="small text-black-50">087764084641</div>
            </div>
          </div>
        </div>
      </div>

      <div class="social d-flex justify-content-center">
        <a href="#" class="mx-2">
          <i class="fab fa-twitter"></i>
        </a>
        <a href="#" class="mx-2">
          <i class="fab fa-facebook-f"></i>
        </a>
        <a href="#" class="mx-2">
          <i class="fab fa-github"></i>
        </a>
      </div>

    </div>
  </section>

  <!-- Footer -->
  <footer class="bg-black small text-center text-white-50">
    <div class="container">
      Copyright &copy;RiyaniKoswara2019
    </div>
  </footer>
</body>
</html>