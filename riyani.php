<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Curiculum Vitae Riyani Koswara</title>

  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="" rel="stylesheet" type="text/css">
  <link href="" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css" type="text/css">
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="css/freelancer.min.css" rel="stylesheet">
</head>

<body>
  <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">Curiculum Vitae</a>
    </div>
  </nav>
  <header class="masthead text-black text-center">
    <div class="container d-flex align-items-center flex-column">
      <img src="senin.jpg" width="200px" height="250px" alt="">
      <h1 class="masthead-heading text-uppercase mb-8"><strong>Riyani Koswara</strong></h1>
      <div class="divider-custom divider-black">
        <div class="divider-custom-line"></div>
        <div class="divider-custom-icon">
          <i class="fas fa-circle"></i>
        </div>
        <div class="divider-custom-line"></div>
      </div>


      <h4 class="masthead-subheading font-weight-light mb-2">Student | Senior High School</h4>
        </div>
      </div>
    </div>
  </div>
</section>
    </div>
  </header>

     <div class="container-fluid">
      <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0"><i class="fas fa-graduation-cap">Education</i></h2>
      <div class="divider-custom divider-black">
        <div class="divider-custom-line"></div>
        <div class="divider-custom-icon">
          <i class="fas fa-circle"></i>
        </div>
        <div class="divider-custom-line"></div>
      </div>
        <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">

              <div class="info-box-content">
                <center>
                  <div class="text-center text-black">
                <i class="fas fa-child fa-8x"></i>
              </div>
                <span class="info-box-text" style="font-family: Footlight MT Light; font-size: 20px;">2008-2009<br> <strong>TK AISYIYAH BUSTANUL ATHFAL II BADAKARYA </strong></span>
              </center>
              </div>
            </div>
          </div>

          <div class="col-12 col-sm-9 col-md-3">
            <div class="info-box">
              <div class="info-box-content">
                <center>
                  <div class="text-center text-black">
                <i class="fas fa-briefcase fa-8x"></i>
              </div>
                <span class="info-box-text" style="font-family:Footlight MT Light; font-size: 20px;">2009-2015<br>
                 <strong>SD NEGERI 1 BADAKARYA </strong></span>
              </center>
                <span class="info-box-number">
                </span>
              </div>
            </div>
          </div>

          <div class="col-12 col-sm-10 col-md-3">
            <div class="info-box">
              <div class="info-box-content">
                <center>
                  <div class="text-center text-black">
                <i class="fas fa-suitcase fa-8x"></i>
              </div>
                <span class="info-box-text" style="font-family:Footlight MT Light; font-size: 20px;">2015-2018<br>
                 <strong>SMP NEGERI 1 WANADADI </strong></span>
              </center>
                <span class="info-box-number">
                </span>
              </div>
            </div>
          </div>

          <div class="col-12 col-sm-9 col-md-3">
            <div class="info-box">
              <div class="info-box-content">
                <center>
                  <div class="text-center text-black">
                <i class="fas fa-building fa-8x"></i>
              </div>
                <span class="info-box-text" style="font-family:Footlight MT Light; font-size: 20px;">2018-2021<br>
                 <strong>SMK NEGERI 1 BAWANG </strong></span>
              </center>
                <span class="info-box-number">
                </span>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div class="container">

      <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Portfolio</h2>

      <div class="divider-custom">
        <div class="divider-custom-line"></div>
        <div class="divider-custom-icon">
          <i class="fas fa-circle"></i>
        </div>
        <div class="divider-custom-line"></div>
      </div>

      <div class="row">

        <div class="col-10 col-sm-5 col-md-3">
            <center>
            <img src="futsal.jpg" width="200px" height="250px"><br>
            <strong style="font-size: 20px;">Logo Futsal</strong>
          </center>
        </div>

    <div class="col-10 col-sm-9 col-md-3">
            <center>
            <img src="vec.jpg" width="200px" height="250px"><br>
            <strong style="font-size: 20px;">Vector Wajah</strong>
          </center>   
           </div>  

       <div class="col-10 col-sm-11 col-md-3">
             <center>
            <img src="ilmu.jpg" width="200px" height="250px"><br>
            <strong style="font-size: 20px;">Brosur</strong>
          </center> 
           </div>  

      <div class="col-10 col-sm-11 col-md-3">
             <center>
            <img src="span.jpg" width="200px" height="300px"><br>
            <strong style="font-size: 20px;">Spanduk</strong>
          </center> 
           </div>  


    </div>
  <div class="page-section bg-secondary text-white mb-0">
    <div class="container">

      <h2 class="page-section-heading text-center text-uppercase text-white"><i class="fas fa-user-circle">About Me</i></h2>

      <div class="divider-custom divider-light">
        <div class="divider-custom-line"></div>
        <div class="divider-custom-icon">
          <i class="fas fa-circle"></i>
        </div>
        <div class="divider-custom-line"></div>
      </div>

      <div class="row">
        <div class="col-lg-4 ml-auto">
          <p class="lead">Nama lengkap saya adalah Riyani Koswara.Lahir di Banjarnegara,24 April 2003.Sedang melaksanakan pendidikan di SMK Negeri 1 Bawang jurusan Rekayasa Perangkat Lunak. Saya memiliki berat badan 44kg dan tinggi badan 155cm.</p>
        </div>
        <div class="col-lg-4 mr-auto">
          <p class="lead">Saya merupakan seseorang yang tertarik dengan website dan design. Memiliki hoby menyanyi dan berolahraga. Saya seseorang yang memiliki mimpi setinggi langit. Saya memiliki prinsip bahwa tidak ada yang tidak mungkin di dunia ini selagi masih mau berusaha dan berdoa.</p>
        </div>
      </div>
    </div>
  </div>   

  <section class="page-section" id="contact">
    <div class="container">

      <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Contact Me</h2>

      <div class="divider-custom">
        <div class="divider-custom-line"></div>
        <div class="divider-custom-icon">
          <i class="fas fa-circle"></i>
        </div>
        <div class="divider-custom-line"></div>
      </div>

      <div class="row">
        <div class="col-lg-8 mx-auto">
          <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
          <form name="sentMessage" id="contactForm" novalidate="novalidate">
            <div class="control-group">
              <div class="form-group floating-label-form-group controls mb-0 pb-2">
                <label>Name</label>
                <input class="form-control" id="name" type="text" placeholder="Name" required="required" data-validation-required-message="Please enter your name.">
                <p class="help-block text-danger"></p>
              </div>
            </div>
            <div class="control-group">
              <div class="form-group floating-label-form-group controls mb-0 pb-2">
                <label>Email Address</label>
                <input class="form-control" id="email" type="email" placeholder="Email Address" required="required" data-validation-required-message="Please enter your email address.">
                <p class="help-block text-danger"></p>
              </div>
            </div>
            <div class="control-group">
              <div class="form-group floating-label-form-group controls mb-0 pb-2">
                <label>Phone Number</label>
                <input class="form-control" id="phone" type="tel" placeholder="Phone Number" required="required" data-validation-required-message="Please enter your phone number.">
                <p class="help-block text-danger"></p>
              </div>
            </div>
            <div class="control-group">
              <div class="form-group floating-label-form-group controls mb-0 pb-2">
                <label>Message</label>
                <textarea class="form-control" id="message" rows="5" placeholder="Message" required="required" data-validation-required-message="Please enter a message."></textarea>
                <p class="help-block text-danger"></p>
              </div>
            </div>
            <br>
            <div id="success"></div>
            <div class="form-group">
              <center>
              <button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton">Send</button>
            </center>
            </div>
          </form>
        </div>
      </div>

    </div>
  </section>

  <footer class="footer text-center">
    <div class="container">
      <div class="row">

        <div class="col-lg-4 mb-5 mb-lg-0">
          <a class="btn btn-outline-light btn-social mx-1" href="#">
            <i class="fas fa-home"></i></a>
          <h4 class="text-uppercase mb-4">Location</h4>
          <p class="lead mb-0">Banjarnegara,Punggelan
            <br>Badakarya, RT 01 RW 03</p>
        </div>

        <div class="col-lg-4 mb-5 mb-lg-0">
          <h4 class="text-uppercase mb-4">My Contact</h4>
          <a class="btn btn-outline-light btn-social mx-1" href="#">
            <i class="fas fa-camera-retro"></i>
          </a>
  
          <a class="btn btn-outline-light btn-social mx-1" href="#">
            <i class="fas fa-phone"></i>
          </a>
          <a class="btn btn-outline-light btn-social mx-1" href="#">
            <i class="fas fa-phone-square"></i>
          </a>
  
        </div>

      </div>
    </div>
  </footer>

  <section class="copyright py-4 text-center text-white">
    <div class="container">
      <small>Copyright &copy;Sikos2019</small>
    </div>
  </section>

   <div class="scroll-to-top d-lg-none position-fixed ">
    <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
      <i class="fa fa-chevron-up"></i>
    </a>
  </div>

  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="js/jqBootstrapValidation.js"></script>
  <script src="js/contact_me.js"></script>
  <script src="js/freelancer.min.js"></script>

</body>

</html>
